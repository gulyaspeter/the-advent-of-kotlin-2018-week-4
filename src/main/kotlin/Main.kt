import java.lang.reflect.Method
import java.lang.reflect.Proxy
import kotlin.reflect.jvm.kotlinFunction

class ObjectIdentifierToken

enum class Recording {
    YES, NO
}

var recording = Recording.NO
var mockedFunctions = mutableMapOf<String, () -> Any?>()
lateinit var currentMockedFunction: () -> Any?

private val defaultValues = mutableMapOf<Class<*>?, Any>(
    Byte::class.java to Byte.MIN_VALUE,
    Short::class.java to Short.MIN_VALUE,
    Int::class.java to Int.MIN_VALUE,
    Long::class.java to Long.MIN_VALUE,
    Float::class.java to Float.MIN_VALUE,
    Double::class.java to Double.MIN_VALUE,
    Boolean::class.java to false,
    Char::class.java to Char.MIN_VALUE
)

fun defineDefaultValue(klass: Class<*>, defaultValue: Any) {
    defaultValues[klass] = defaultValue
}

inline fun <reified T> mock(): T {
    val clazz = T::class.java
    val token = ObjectIdentifierToken()
    return Proxy.newProxyInstance(
        clazz.classLoader,
        arrayOf(clazz),
        createInvocationHandler(token)
    ) as T
}

fun createInvocationHandler(token: ObjectIdentifierToken) = { proxy: Any, method: Method, args: Array<Any>? ->
    when (recording) {
        Recording.YES -> recording(token, method, args)
        else -> execute(token, method, args)
    }
}

private fun recording(token: ObjectIdentifierToken, method: Method, args: Array<Any>?): Any? {
    val functionSyntax = createFunctionSyntax(token, method, args)
    mockedFunctions.put(functionSyntax, currentMockedFunction)
    return defaultValues[method.returnType]
}

private fun execute(token: ObjectIdentifierToken, method: Method, args: Array<Any>?): Any? {
    val functionSyntax = createFunctionSyntax(token, method, args)
    return if (mockedFunctions.contains(functionSyntax)) {
        mockedFunctions[functionSyntax]?.invoke()
    } else {
        defaultValues[method.returnType]
    }
}

fun createFunctionSyntax(token: ObjectIdentifierToken, method: Method, args: Array<Any>?): String {
    var ret = "$token-${method.kotlinFunction}"
    if (args != null) {
        for (arg in args) {
            ret += "[$arg]"
        }
    }
    return ret
}

inline fun <reified T> setReturnValue(noinline call: () -> T, value: T) {
    setBody(call, { value })
}

inline fun <reified T> setBody(noinline call: () -> T, noinline body: () -> T) {
    recording = Recording.YES
    currentMockedFunction = body
    call.invoke()
    recording = Recording.NO
}
