interface Animal {
    fun getWeight(): Int
    fun call(name: String): String
    fun bringHere(): Toy
}