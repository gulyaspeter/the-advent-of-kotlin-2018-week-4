import org.junit.Assert.assertEquals
import org.junit.Test

class MockTest {

    @Test
    fun main() {
        val a = mock<Example>()
        setReturnValue({ a.getInt() }, 2)
        assertEquals(2, a.getInt())

        var i = 1
        val b = mock<Example>()
        setBody({ b.getInt() }, { i++ })
        assertEquals(1, b.getInt())
        assertEquals(2, b.getInt())
        assertEquals(3, b.getInt())
    }

    @Test
    fun getWeightNotInitialised() {
        val goat = mock<Animal>()
        assertEquals(Int.MIN_VALUE, goat.getWeight())
    }

    @Test
    fun getWeight() {
        val goat = mock<Animal>()
        setReturnValue({ goat.getWeight() }, 25)
        assertEquals(25, goat.getWeight())
    }

    @Test
    fun multipleMockGetWeight() {
        val goat = mock<Animal>()
        val cow = mock<Animal>()
        setReturnValue({ goat.getWeight() }, 25)
        setReturnValue({ cow.getWeight() }, 55)

        assertEquals(25, goat.getWeight())
        assertEquals(55, cow.getWeight())
    }

    @Test
    fun useParameter() {
        val dog = mock<Animal>()
        setReturnValue({ dog.call("Come here Wolfie") }, "Wuf, wuf")

        assertEquals("Wuf, wuf", dog.call("Come here Wolfie"))
        assertEquals(null, dog.call("Come here Dogie"))
    }

    class Stick(override var name: String) : Toy

    @Test
    fun customDefaultValue() {
        val dog = mock<Animal>()
        val oakStick = Stick("Oka stick")
        defineDefaultValue(Toy::class.java, oakStick)

        assertEquals(oakStick, dog.bringHere())
    }
}
